clear; clc; close all;
%% Edit this part before running the code:

% Set the number of subject 
subN = 561;

% Set your test data directiory
yourTestDataDir = 'C:MIAP\Project\test data';

% Set your Healthy sample directory
youtHealthySampleDir = 'C:MIAP\Project\subjects\Healthy_sample';

%% Load files
% load subject images files
subFN = ['S', num2str(subN)];
imFN = ['pat', num2str(subN), '.nii'];
imSegFN = ['pat', num2str(subN), '_label.nii'];
addpath(fullfile(yourTestDataDir, subFN))
im = double(niftiread(imFN));
imSeg = double(niftiread(imSegFN));

if subN == 593 || subN == 605, imSeg = imSeg - 1; end

% load Healthy images files
addpath(youtHealthySampleDir);
imH = double(niftiread('00.nii'));
imSegH = double(niftiread('00_mask.nii'));

%% Select Vertebra
% select only vertebra sL to eL
sL = 20; eL = 24;
imSeg = selectVertebra(imSeg, sL, eL);
imSegH = selectVertebra(imSegH, sL, eL);
%% Vertebra Seperation
% seperate each vertebra coordinates for subject
for i = sL: eL, vSub{i}=struct; vSub{i}.x = []; vSub{i}.y = []; vSub{i}.z = []; end
for i = 1: size(imSeg, 3)
    tempIm = imSeg(:, :, i);
    for iv = sL: eL
        [x, y] = find(tempIm == iv);
        vSub{iv}.x = [vSub{iv}.x; x];     %x
        vSub{iv}.y = [vSub{iv}.y; y];     %y
        vSub{iv}.z = [vSub{iv}.z; i*ones(length(x), 1)];     %z
    end 
end

% seperate each vertebra coordinates for healthy image
for i = sL: eL, vH{i}=struct; vH{i}.x = []; vH{i}.y = []; vH{i}.z = []; end
for i = 1: size(imSegH, 3)
    tempIm = imSegH(:, :, i);
    for iv = sL: eL
        [x, y] = find(tempIm == iv);
        vH{iv}.x = [vH{iv}.x; x];     %x
        vH{iv}.y = [vH{iv}.y; y];     %y
        vH{iv}.z = [vH{iv}.z; i*ones(length(x), 1)];     %z
    end 
end
clear tempIm x y i

% plot each vertebra to test the seperation:
% for i = sL: 2: eL
% figure;
% plot3(vH{1, i}.x, vH{1, i}.y, vH{1, i}.z, '.');
% end
%% Extract boundries for each vertebra

% subject
for i = sL: eL, vEdgeSub{i}=struct; vEdgeSub{i}.x = []; ...
        vEdgeSub{i}.y = []; vEdgeSub{i}.z = []; end
    
for iv = sL: eL
    zvals = unique(vSub{iv}.z);
    for iz = 1 : length(zvals)
        xvals = vSub{iv}.x(find(vSub{iv}.z == zvals(iz)));
        yvals = vSub{iv}.y(find(vSub{iv}.z == zvals(iz)));
        mat = zeros(size(imSeg, 1), size(imSeg, 2));
        for ip = 1: length(xvals)
            mat(xvals(ip), yvals(ip)) = 1;
        end
        matEdge = edge(mat, 'Sobel');
        [xEdge yEdge] = find(matEdge);
        vEdgeSub{iv}.x = [vEdgeSub{iv}.x; xEdge];
        vEdgeSub{iv}.y = [vEdgeSub{iv}.y; yEdge];
        vEdgeSub{iv}.z = [vEdgeSub{iv}.z; zvals(iz) * ones(length(xEdge), 1)];
    end
end

% Healthy Image:
for i = sL: eL, vEdgeH{i}=struct; vEdgeH{i}.x = []; ...
        vEdgeH{i}.y = []; vEdgeH{i}.z = []; end
    
for iv = sL: eL
    zvals = unique(vH{iv}.z);
    for iz = 1 : length(zvals)
        xvals = vH{iv}.x(find(vH{iv}.z == zvals(iz)));
        yvals = vH{iv}.y(find(vH{iv}.z == zvals(iz)));
        mat = zeros(size(imH, 1), size(imH, 2));
        for ip = 1: length(xvals)
            mat(xvals(ip), yvals(ip)) = 1;
        end
        matEdge = edge(mat, 'Sobel');
        [xEdge yEdge] = find(matEdge);
        vEdgeH{iv}.x = [vEdgeH{iv}.x; xEdge];
        vEdgeH{iv}.y = [vEdgeH{iv}.y; yEdge];
        vEdgeH{iv}.z = [vEdgeH{iv}.z; zvals(iz) * ones(length(xEdge), 1)];
    end
end
clear iv iz ip mat matEdge xEdge yEdge xvals yvals zvals

% plot one vertebra to test the seperation:
% for i = sL: 3: eL
% figure;
% subplot(1, 2, 1);
% plot3(vEdgeSub{1, i}.x, vEdgeSub{1, i}.y, vEdgeSub{1, i}.z, '.');
% subplot(1, 2, 2);
% plot3(vSub{1, i}.x, vSub{1, i}.y, vSub{1, i}.z, '.');
% end
%% point cloud
% create 3D subject point cloud
r = 10;
[x y z] = ind2sub(size(imSeg), find(imSeg));
subPC = pointCloud([x y z]);
subPC_DS = pcdownsample(subPC, 'nonuniformGridSample', 6*r);

% create 3D Healthy image point cloud
[x y z] = ind2sub(size(imSegH), find(imSegH));
HPC = pointCloud([x y z]);
HPC_DS = pcdownsample(HPC, 'nonuniformGridSample', 6*r);
clear x y z

% create overall subject point cloud from edges:
subEdgePC = [];
for iv = sL: eL
    if ~isempty(vEdgeSub{iv}.x)
        subEdgePC = [subEdgePC; [vEdgeSub{iv}.x, vEdgeSub{iv}.y, vEdgeSub{iv}.z]];
    end
end
subEdgePC = pointCloud(subEdgePC);
subEdgePC_DS = pcdownsample(subEdgePC, 'nonuniformGridSample', r);

% create overall healthy image point cloud from edges:
HEdgePC = [];
for iv = sL: eL
    if ~isempty(vEdgeH{iv}.x)
        HEdgePC = [HEdgePC; [vEdgeH{iv}.x, vEdgeH{iv}.y, vEdgeH{iv}.z]];
    end
end
HEdgePC = pointCloud(HEdgePC);
HEdgePC_DS = pcdownsample(HEdgePC, 'nonuniformGridSample', r);

% create point cloud cell for subject:
for iv = sL: eL
    if ~isempty(vEdgeSub{iv}.x)
        vSubPC{iv} = pointCloud([vEdgeSub{iv}.x, vEdgeSub{iv}.y, vEdgeSub{iv}.z]);
        vSubPC_DS{iv} = pcdownsample(vSubPC{iv}, 'nonuniformGridSample', r);
    else
        disp(['Subject ',num2str(subN),' does not have ',num2str(iv),'th vertebra.'])
    end
end

% create point cloud cell for healthy image:
for iv = sL: eL
    if ~isempty(vEdgeH{iv}.x)
        vHPC{iv} = pointCloud([vEdgeH{iv}.x, vEdgeH{iv}.y, vEdgeH{iv}.z]);
        vHPC_DS{iv} = pcdownsample(vHPC{iv}, 'nonuniformGridSample', r);
    else
        disp(['Healthy image does not have ',num2str(iv),'th vertebra.'])
    end
end
clear iv
%% plot point clouds
figure;
subplot(1, 2, 1); pcshow(subPC); title('Subject');
subplot(1, 2, 2); pcshow(HPC); title('Healthy');

for iv = sL: eL
    if ~isempty(vSubPC{iv}) && ~isempty(vHPC{iv})
        figure;
        subplot(2,2,1); pcshow(vSubPC{iv}); title(['Subject ', num2str(subN)]);
        subplot(2,2,2); pcshow(vSubPC_DS{iv}); title(['Down Sampled Subject, r = ',num2str(r)])
        subplot(2,2,3); pcshow(vHPC{iv}); title('Healthy Image');
        subplot(2,2,4); pcshow(vHPC_DS{iv}); 
        title(['Down Sampled Healthy Image, r = ',num2str(r)])
        sgtitle(['Point Clound Plot - vertebra: ', num2str(iv)], 'color', [1 1 1]);
    end
end
clear iv
%% REGISTRATION METHODS:
%% Method 1: CPD Total Registration
% total CPD registration
disp(['Method 1: Overall CPD Registration on Subject ', num2str(subN), ' Image']); 
moving = subPC_DS;
fixed = HPC_DS;
tform = pcregistercpd(moving, fixed, 'verbose', true);
subCPD = pctransform(moving, tform); 
clear tform moving fixed
%% Method 1: Interpolate Total CPD
x = subPC_DS.Location(:, 1);
y = subPC_DS.Location(:, 2);
z = subPC_DS.Location(:, 3);
xp = subCPD.Location(:, 1);
yp = subCPD.Location(:, 2);
zp = subCPD.Location(:, 3);

xq = []; yq = []; zq = [];
for iv = sL: eL
    xq = [xq; vSub{iv}.x];
    yq = [yq; vSub{iv}.y];
    zq = [zq; vSub{iv}.z];
end

disp('Interpolating Total CPD x coordinate data')
interpX = griddata(x, y, z, xp, xq, yq, zq, 'natural');
disp('Interpolating Total CPD y coordinate data')
interpY = griddata(x, y, z, yp, xq, yq, zq, 'natural');
disp('Interpolating Total CPD z coordinate data')
interpZ = griddata(x, y, z, zp, xq, yq, zq, 'natural');

subTotalCPDInterp = pointCloud([interpX, interpY, interpZ]);
clear x y z xp yp zp xq yq zq interpX interpY interpZ
%% Method 1: plot Total CPD results
% Overall Edge
figure; subplot(1, 3, 1); pcshow(subEdgePC_DS); title('Moving Image');
subplot(1, 3, 2); pcshow(subCPD); title('CPD Registration');
subplot(1, 3, 3); pcshow(HEdgePC_DS); title('Fixed Image');
sgtitle(['Subject ', num2str(subN), ' Overall CPD Registration'], 'color', [1 1 1]);

% Overall Interpolation
figure; subplot(1, 3, 1); pcshow(subPC); title('Moving Image');
subplot(1, 3, 2); pcshow(subTotalCPDInterp); title('CPD Registration');
subplot(1, 3, 3); pcshow(HPC); title('Fixed Image');
sgtitle(['Subject ', num2str(subN), ' Overall CPD Registration Interpolation'], ...
    'color', [1 1 1]);
%% Method 1: ICP Total Registration
% total ICP registration 
disp(['Method 1: Overall ICP Registration on Subject ', num2str(subN), ' Image']);
moving = subPC_DS;
fixed = HPC_DS;
moving.Normal = pcnormals(moving);
fixed.Normal = pcnormals(fixed);
tform = pcregistericp(moving, fixed);
subICP = pctransform(moving, tform);
clear tform
%% Method 1: Interpolate Total ICP
x = subPC_DS.Location(:, 1);
y = subPC_DS.Location(:, 2);
z = subPC_DS.Location(:, 3);
xp = subICP.Location(:, 1);
yp = subICP.Location(:, 2);
zp = subICP.Location(:, 3);

xq = []; yq = []; zq = [];
for iv = sL: eL
    xq = [xq; vSub{iv}.x];
    yq = [yq; vSub{iv}.y];
    zq = [zq; vSub{iv}.z];
end

disp('Interpolating Total ICP x coordinate data')
interpX = griddata(x, y, z, xp, xq, yq, zq, 'natural');
disp('Interpolating Total ICP y coordinate data')
interpY = griddata(x, y, z, yp, xq, yq, zq, 'natural');
disp('Interpolating Total ICP z coordinate data')
interpZ = griddata(x, y, z, zp, xq, yq, zq, 'natural');

subTotalICPInterp = pointCloud([interpX, interpY, interpZ]);
clear x y z xp yp zp xq yq zq interpX interpY interpZ
%% Method 1: plot Total ICP results
% Overall Edge
figure; subplot(1, 3, 1); pcshow(subEdgePC_DS); title('Moving Image');
subplot(1, 3, 2); pcshow(subICP); title('ICP Registration');
subplot(1, 3, 3); pcshow(HEdgePC_DS); title('Fixed Image');
sgtitle(['Subject ', num2str(subN), ' Overall ICP Registration'], 'color', [1 1 1]);

% Overall Interpolation
figure; subplot(1, 3, 1); pcshow(subPC); title('Moving Image');
subplot(1, 3, 2); pcshow(subTotalICPInterp); title('ICP Registration');
subplot(1, 3, 3); pcshow(HPC); title('Fixed Image');
sgtitle(['Subject ', num2str(subN), ' Overall ICP Registration Interpolation'], ...
    'color', [1 1 1]);
%% Method 2: CPD vertebra Registration 
% vertebra by vertebra
disp(['Method 2: Vertebra CPD Registration on Subject ', num2str(subN), ' Image']);
for iv = sL: eL
    if ~isempty(vHPC_DS{iv}) && ~isempty(vSubPC_DS{iv})
        disp(['Vertebra ', num2str(iv)])
        moving = vSubPC_DS{iv};
        fixed = vHPC_DS{iv};
        tformCPD{iv} = pcregistercpd(moving, fixed);
        vSubCPD{iv} = pctransform(moving, tformCPD{iv});
    else
        disp(['Cannot register vertebra ', num2str(iv)])
    end
end
%% Method 2: plot CPD results per vertebra
for iv = sL: eL
    if ~isempty(vHPC_DS{iv}) && ~isempty(vSubPC_DS{iv})
        figure;
        subplot(1, 3, 1); pcshowpair(vSubCPD{iv}, vHPC{iv}, 'MarkerSize', 50)
        subplot(1, 3, 2); pcshow(vSubCPD{iv}); title('Registered Moving');
        subplot(1, 3, 3); pcshow(vHPC{iv}); title('Fixed');
        sgtitle(['Subject ',num2str(subN),', Vertebra ',num2str(iv),' registration'], ...
            'color', [1 1 1]);
    else
        disp(['Cannot register vertebra ', num2str(iv)])
    end
end
%% Method 2: Reconstruct the spine from CPD registered vertebras
spineCPDreg = [];
for iv = sL: eL
    spineCPDreg = [spineCPDreg; vSubCPD{iv}.Location];
end
spineCPDregPC = pointCloud([spineCPDreg(:,1), spineCPDreg(:,2), spineCPDreg(:,3)]);
clear iv
%% Method 2: plot CPD vertebra registration reconstruction results
% figure;
% plot3(spineCPDreg(:,1), spineCPDreg(:,2), spineCPDreg(:,3), '.')

%vertebra by vertebra
figure; subplot(1, 3, 1); pcshow(subEdgePC_DS); title('Moving Image');
subplot(1, 3, 2); pcshow(spineCPDregPC); title('CPD Registration');
subplot(1, 3, 3); pcshow(HEdgePC_DS); title('Fixed Image');
sgtitle(['Subject ', num2str(subN), ' Vertebra Registration'], 'color', [1 1 1]);
%% Method 2: Vertebra CPD Interpolation
x = []; xp = []; y = []; yp = []; z = []; zp = [];
for iv = sL: eL
    xp = [xp; vSubCPD{iv}.Location(:, 1)];
    x = [x; vSubPC_DS{iv}.Location(:, 1)];
    yp = [yp; vSubCPD{iv}.Location(:, 2)];
    y = [y; vSubPC_DS{iv}.Location(:, 2)];
    zp = [zp; vSubCPD{iv}.Location(:, 3)];
    z = [z; vSubPC_DS{iv}.Location(:, 3)];
end
xq = []; yq = []; zq = [];
for iv = sL: eL
    xq = [xq; vSub{iv}.x];
    yq = [yq; vSub{iv}.y];
    zq = [zq; vSub{iv}.z];
end

disp('Interpolating CPD x coordinate data')
interpX = griddata(x, y, z, xp, xq, yq, zq, 'natural');
disp('Interpolating CPD y coordinate data')
interpY = griddata(x, y, z, yp, xq, yq, zq, 'natural');
disp('Interpolating CPD z coordinate data')
interpZ = griddata(x, y, z, zp, xq, yq, zq, 'natural');

subCPDInterp = pointCloud([interpX, interpY, interpZ]);
%% Method 2: plot Vertebra CPD interpolated registration
figure; subplot(1, 3, 1); pcshow(subPC); title('Moving Image');
subplot(1, 3, 2); pcshow(subCPDInterp); title('CPD Registration');
subplot(1, 3, 3); pcshow(HPC); title('Fixed Image');
sgtitle(['Subject ', num2str(subN), ' Vertebra Registration Interpolation'], ...
    'color', [1 1 1]);
%% Method 2: ICP vertebra Registration
% vertebra by vertebra
disp(['Method 2: Vertebra ICP Registration on Subject ', num2str(subN), ' Image']);
for iv = sL: eL
    if ~isempty(vHPC_DS{iv}) && ~isempty(vSubPC_DS{iv})
        disp(['Vertebra ', num2str(iv)])
        moving = vSubPC_DS{iv};
        fixed = vHPC_DS{iv};
        moving.Normal = pcnormals(moving);
        fixed.Normal = pcnormals(fixed);
        tformICP{iv} = pcregistericp(moving, fixed);
        vSubICP{iv} = pctransform(moving, tformICP{iv});
    else
        disp(['Cannot register vertebra ', num2str(iv)])
    end
end
clear tform
%% Method 2: plot ICP results per vertebra
for iv = sL: eL
    if ~isempty(vHPC_DS{iv}) && ~isempty(vSubPC_DS{iv})
        figure;
        subplot(1, 3, 1); pcshowpair(vSubICP{iv}, vHPC{iv}, 'MarkerSize', 50)
        subplot(1, 3, 2); pcshow(vSubICP{iv}); title('Registered Moving');
        subplot(1, 3, 3); pcshow(vHPC{iv}); title('Fixed');
        sgtitle(['Subject ',num2str(subN),', Vertebra ',num2str(iv),' registration'], ...
            'color', [1 1 1]);
    else
        disp(['Cannot register vertebra ', num2str(iv)])
    end
end
%% Method 2: Reconstruct the spine from ICP registered vertebras
spineICPreg = [];
for iv = sL: eL
    spineICPreg = [spineICPreg; vSubICP{iv}.Location];
end
spineICPregPC = pointCloud([spineICPreg(:,1), spineICPreg(:,2), spineICPreg(:,3)]);
%% Method 2: plot ICP vertebra registration reconstruction results
% figure;
% plot3(spineICPreg(:,1), spineICPreg(:,2), spineICPreg(:,3), '.') 

%vertebra by vertebra
figure; subplot(1, 3, 1); pcshow(subEdgePC_DS); title('Moving Image');
subplot(1, 3, 2); pcshow(spineICPregPC); title('ICP Registration');
subplot(1, 3, 3); pcshow(HEdgePC_DS); title('Fixed Image');
sgtitle(['Subject ', num2str(subN),' Vertebra Registration'], 'color', [1 1 1]);
%% Method 2: Vertebra ICP Interpolation
x = []; xp = []; y = []; yp = []; z = []; zp = [];
for iv = sL: eL
    xp = [xp; vSubICP{iv}.Location(:, 1)];
    x = [x; vSubPC_DS{iv}.Location(:, 1)];
    yp = [yp; vSubICP{iv}.Location(:, 2)];
    y = [y; vSubPC_DS{iv}.Location(:, 2)];
    zp = [zp; vSubICP{iv}.Location(:, 3)];
    z = [z; vSubPC_DS{iv}.Location(:, 3)];
end
xq = []; yq = []; zq = [];
for iv = sL: eL
    xq = [xq; vSub{iv}.x];
    yq = [yq; vSub{iv}.y];
    zq = [zq; vSub{iv}.z];
end

disp('Interpolating ICP x coordinate data')
interpX = griddata(x, y, z, xp, xq, yq, zq, 'natural');
disp('Interpolating ICP y coordinate data')
interpY = griddata(x, y, z, yp, xq, yq, zq, 'natural');
disp('Interpolating ICP z coordinate data')
interpZ = griddata(x, y, z, zp, xq, yq, zq, 'natural');

subICPInterp = pointCloud([interpX, interpY, interpZ]);
clear iv i intepX interpY interpZ x xp xq y yp yq z zp zq
%% Method 2: plot vertebra ICP interpolated registration
figure; subplot(1, 3, 1); pcshow(subPC); title('Moving Image');
subplot(1, 3, 2); pcshow(subICPInterp); title('ICP Registration');
subplot(1, 3, 3); pcshow(HPC); title('Fixed Image');
sgtitle(['Subject ', num2str(subN), ' Vertebra Registration Interpolation'], ...
    'color', [1 1 1]);
%% Method 3: Total ICP then CPD
disp(['Method 3: Overall ICP then CPD Registration on Subject ', num2str(subN), ' Image']); 
moving = subICP;
moving = pcdownsample(moving, 'nonuniformGridSample', r);
fixed = HPC_DS;
fixed = pcdownsample(fixed, 'nonuniformGridSample', r);
tform = pcregistercpd(moving, fixed, 'verbose', false);
subTotalICPthenCPD = pctransform(moving, tform);
clear tform fixed
%% Method 3: Interpolate Total ICP then CPD
x = moving.Location(:, 1);
y = moving.Location(:, 2);
z = moving.Location(:, 3);
xp = subTotalICPthenCPD.Location(:, 1);
yp = subTotalICPthenCPD.Location(:, 2);
zp = subTotalICPthenCPD.Location(:, 3);

xq = []; yq = []; zq = []; 
for iv = sL: eL
    xq = [xq; vSub{iv}.x];
    yq = [yq; vSub{iv}.y];
    zq = [zq; vSub{iv}.z];
end

disp('Interpolating Total ICP then CPD x coordinate data')
interpX = griddata(x, y, z, xp, xq, yq, zq, 'natural');
disp('Interpolating Total ICP then CPD y coordinate data')
interpY = griddata(x, y, z, yp, xq, yq, zq, 'natural');
disp('Interpolating Total ICP then CPD z coordinate data')
interpZ = griddata(x, y, z, zp, xq, yq, zq, 'natural');

subTotalICPthenCPDInterp = pointCloud([interpX, interpY, interpZ]);
clear x y z xp yp zp xq yq zq interpX interpY interpZ
%% Method 3: plot Total ICP then CPD results
% Overall Edge
figure; subplot(1, 3, 1); pcshow(subEdgePC_DS); title('Moving Image');
subplot(1, 3, 2); pcshow(subTotalICPthenCPD); title('ICP then CPD Registration');
subplot(1, 3, 3); pcshow(HEdgePC_DS); title('Fixed Image');
sgtitle(['Subject ', num2str(subN), ' Overall ICP then CPD Registration'], ...
    'color', [1 1 1]);

% Overall Interpolation
figure; subplot(1, 3, 1); pcshow(subPC); title('Moving Image');
subplot(1, 3, 2); pcshow(subTotalICPthenCPDInterp); title('ICP then CPD Registration');
subplot(1, 3, 3); pcshow(HPC); title('Fixed Image');
sgtitle(['Subject ', num2str(subN), ...
    ' Overall ICP then CPD Registration Interpolation'], 'color', [1 1 1]);
%% Method 3: Vertebra ICP then CPD registration
clear tform
% CPD registration on each ICP registered vertebra
disp(['Method 3: Vertebra ICP then CPD Registration on Subject ', num2str(subN), ' Image'])
for iv = sL: eL
    disp(['Vertebra ', num2str(iv)]);
    moving = vSubICP{iv};
    fixed = vHPC_DS{iv};
    moving.Normal = pcnormals(moving);
    fixed.Normal = pcnormals(fixed);
    tform{iv} = pcregistericp(moving, fixed, 'verbose', false);
    vSubICPthenCPD{iv} = pctransform(moving, tform{iv});
end
%% Method 3: Interpolate Vertebra ICP then CPD
for iv = sL: eL
    disp(['interpolating Vertebra ', num2str(iv)]);
    x = vSubPC_DS{iv}.Location(:, 1);
    y = vSubPC_DS{iv}.Location(:, 2);
    z = vSubPC_DS{iv}.Location(:, 3);
    xp = vSubICP{iv}.Location(:, 1);
    yp = vSubICP{iv}.Location(:, 2);
    zp = vSubICP{iv}.Location(:, 3);
    xq = vSub{iv}.x;
    yq = vSub{iv}.y;
    zq = vSub{iv}.z;
    interpX = griddata(x, y, z, xp, xq, yq, zq, 'natural');
    interpY = griddata(x, y, z, yp, xq, yq, zq, 'natural');
    interpZ = griddata(x, y, z, zp, xq, yq, zq, 'natural');
    xqNew{iv} = [interpX, interpY, interpZ];
end
% second interpolation
x = []; y = []; z = []; xp = []; yp = []; zp = []; 
for iv = sL: eL
    x = [x; vSubICP{iv}.Location(:, 1)];
    y = [y; vSubICP{iv}.Location(:, 2)];
    z = [z; vSubICP{iv}.Location(:, 3)];
    xp = [xp; vSubICPthenCPD{iv}.Location(:,1)];
    yp = [yp; vSubICPthenCPD{iv}.Location(:,2)];
    zp = [zp; vSubICPthenCPD{iv}.Location(:,3)];
end
xq = []; yq = []; zq = [];
for iv = sL: eL
    xq = [xq; xqNew{iv}(:, 1)];
    yq = [yq; xqNew{iv}(:, 2)];
    zq = [zq; xqNew{iv}(:, 3)];
end
disp('Interpolating vertebra ICP then CPD x coordinate data')
interpX = griddata(x, y, z, xp, xq, yq, zq, 'natural');
disp('Interpolating vertebra ICP then CPD y coordinate data')
interpY = griddata(x, y, z, yp, xq, yq, zq, 'natural');
disp('Interpolating vertebra ICP then CPD z coordinate data')
interpZ = griddata(x, y, z, zp, xq, yq, zq, 'natural');
vSubICPthenCPDInterp = pointCloud([interpX, interpY, interpZ]);
%% Method 3: plot vertebra ICP then CPD registration interpolation
figure; subplot(1, 3, 1); pcshow(subPC); title('Moving Image');
subplot(1, 3, 2); pcshow(vSubICPthenCPDInterp); title('ICP then CPD Vertebra Registration');
subplot(1, 3, 3); pcshow(HPC); title('Fixed Image');
sgtitle(['Subject ', num2str(subN), ' ICP then CPD Vertebra Registration Interpolation'], ...
    'color', [1 1 1]);
%% PERFORMANCE METRICES:
%% Extract Performance Planes
% extract three lateral, frontal, and axial planes with greatest number of
% data points in the helthy image to apply performance metrices on them

% Lateral planes:
for ip = 1: size(imSegH, 1)
    plane = imSegH(ip, :, :);
    s(ip) = length(find(plane));
end
[~, performancePlane(1)] = max(s);
% Axial planes:
for ip = 1: size(imSegH, 2)
    plane = imSegH(:, ip, :);
    s(ip) = length(find(plane));
end
[~, performancePlane(2)] = max(s);
% Frontal planes:
for ip = 1: size(imSegH, 3)
    plane = imSegH(:, :, ip);
    s(ip) = length(find(plane));
end
[~, performancePlane(3)] = max(s);
clear ip s plane
%% DS
disp('Performing DS Metric');
xq = []; yq = []; zq = [];
for iv = sL: eL
    xq = [xq; vSub{iv}.x];
    yq = [yq; vSub{iv}.y];
    zq = [zq; vSub{iv}.z];
end
locsMoving = [xq, yq, zq];
clear xq yq zq iv

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Dice on method 1 CPD:
locsReg = subTotalCPDInterp.Location;
%%%%%%%%%%%%%%%%%%%%%%%%%% Lateral Plane:
[Similarity.Method1.CPD.Dice.Lateral.After, ...
    Similarity.Method1.CPD.Dice.Lateral.Before] = ...
    myDice('L', performancePlane, locsMoving, locsReg, subTotalCPDInterp, imSeg, imSegH);
%%%%%%%%%%%%%%%%%%%%%%%%%% Axial Plane:
[Similarity.Method1.CPD.Dice.Axial.After, ...
    Similarity.Method1.CPD.Dice.Axial.Before] = ...
    myDice('A', performancePlane, locsMoving, locsReg, subTotalCPDInterp, imSeg, imSegH);
%%%%%%%%%%%%%%%%%%%%%%%%%% Frontal Plane:
[Similarity.Method1.CPD.Dice.Frontal.After, ...
    Similarity.Method1.CPD.Dice.Frontal.Before] = ...
    myDice('F', performancePlane, locsMoving, locsReg, subTotalCPDInterp, imSeg, imSegH);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Dice on method 1 ICP:
locsReg = subTotalICPInterp.Location;
%%%%%%%%%%%%%%%%%%%%%%%%%% Lateral Plane:
[Similarity.Method1.ICP.Dice.Lateral.After, ...
    Similarity.Method1.ICP.Dice.Lateral.Before] = ...
    myDice('L', performancePlane, locsMoving, locsReg, subTotalICPInterp, imSeg, imSegH);
%%%%%%%%%%%%%%%%%%%%%%%%%% Axial Plane:
[Similarity.Method1.ICP.Dice.Axial.After, ...
    Similarity.Method1.ICP.Dice.Axial.Before] = ...
    myDice('A', performancePlane, locsMoving, locsReg, subTotalICPInterp, imSeg, imSegH);
%%%%%%%%%%%%%%%%%%%%%%%%%% Frontal Plane:
[Similarity.Method1.ICP.Dice.Frontal.After, ...
    Similarity.Method1.ICP.Dice.Frontal.Before] = ...
    myDice('F', performancePlane, locsMoving, locsReg, subTotalICPInterp, imSeg, imSegH);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Dice on method 2 Interpolate CPD:
locsReg = subCPDInterp.Location;
%%%%%%%%%%%%%%%%%%%%%%%%%% Lateral Plane:
[Similarity.Method2.Interpolate.CPD.Dice.Lateral.After, ...
    Similarity.Method2.Interpolate.CPD.Dice.Lateral.Before] = ...
    myDice('L', performancePlane, locsMoving, locsReg, subCPDInterp, imSeg, imSegH);
%%%%%%%%%%%%%%%%%%%%%%%%%% Axial Plane:
[Similarity.Method2.Interpolate.CPD.Dice.Axial.After, ...
    Similarity.Method2.Interpolate.CPD.Dice.Axial.Before] = ...
    myDice('A', performancePlane, locsMoving, locsReg, subCPDInterp, imSeg, imSegH);
%%%%%%%%%%%%%%%%%%%%%%%%%% Frontal Plane:
[Similarity.Method2.Interpolate.CPD.Dice.Frontal.After, ...
    Similarity.Method2.Interpolate.CPD.Dice.Frontal.Before] = ...
    myDice('F', performancePlane, locsMoving, locsReg, subCPDInterp, imSeg, imSegH);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Dice on method 2 Interpolate ICP:
locsReg = subICPInterp.Location;
%%%%%%%%%%%%%%%%%%%%%%%%%% Lateral Plane:
[Similarity.Method2.Interpolate.ICP.Dice.Lateral.After, ...
    Similarity.Method2.Interpolate.ICP.Dice.Lateral.Before] = ...
    myDice('L', performancePlane, locsMoving, locsReg, subICPInterp, imSeg, imSegH);
%%%%%%%%%%%%%%%%%%%%%%%%%% Axial Plane:
[Similarity.Method2.Interpolate.ICP.Dice.Axial.After, ...
    Similarity.Method2.Interpolate.ICP.Dice.Axial.Before] = ...
    myDice('A', performancePlane, locsMoving, locsReg, subICPInterp, imSeg, imSegH);
%%%%%%%%%%%%%%%%%%%%%%%%%% Frontal Plane:
[Similarity.Method2.Interpolate.ICP.Dice.Frontal.After, ...
    Similarity.Method2.Interpolate.ICP.Dice.Frontal.Before] = ...
    myDice('F', performancePlane, locsMoving, locsReg, subICPInterp, imSeg, imSegH);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Dice on method 3 Total ICP then CPD:
locsReg = subTotalICPthenCPDInterp.Location;
%%%%%%%%%%%%%%%%%%%%%%%%%% Lateral Plane:
[Similarity.Method3.Total.Dice.Lateral.After, ...
    Similarity.Method3.Total.Dice.Lateral.Before] = ...
    myDice('L', performancePlane, locsMoving, locsReg, subTotalICPthenCPDInterp, imSeg, imSegH);
%%%%%%%%%%%%%%%%%%%%%%%%%% Axial Plane:
[Similarity.Method3.Total.Dice.Axial.After, ...
    Similarity.Method3.Total.Dice.Axial.Before] = ...
    myDice('A', performancePlane, locsMoving, locsReg, subTotalICPthenCPDInterp, imSeg, imSegH);
%%%%%%%%%%%%%%%%%%%%%%%%%% Frontal Plane:
[Similarity.Method3.Total.Dice.Frontal.After, ...
    Similarity.Method3.Total.Dice.Frontal.Before] = ...
    myDice('F', performancePlane, locsMoving, locsReg, subTotalICPthenCPDInterp, imSeg, imSegH);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Dice on method 3 Vertebra ICP then CPD:
locsReg = vSubICPthenCPDInterp.Location;
%%%%%%%%%%%%%%%%%%%%%%%%%% Lateral Plane:
[Similarity.Method3.Vertebra.Dice.Lateral.After, ...
    Similarity.Method3.Vertebra.Dice.Lateral.Before] = ...
    myDice('L', performancePlane, locsMoving, locsReg, vSubICPthenCPDInterp, imSeg, imSegH);
%%%%%%%%%%%%%%%%%%%%%%%%%% Axial Plane:
[Similarity.Method3.Vertebra.Dice.Axial.After, ...
    Similarity.Method3.Vertebra.Dice.Axial.Before] = ...
    myDice('A', performancePlane, locsMoving, locsReg, vSubICPthenCPDInterp, imSeg, imSegH);
%%%%%%%%%%%%%%%%%%%%%%%%%% Frontal Plane:
[Similarity.Method3.Vertebra.Dice.Frontal.After, ...
    Similarity.Method3.Vertebra.Dice.Frontal.Before] = ...
    myDice('F', performancePlane, locsMoving, locsReg, vSubICPthenCPDInterp, imSeg, imSegH);

%% ASD  
disp('Performing ASD Metric');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ASD on method 1 CPD:
%%%%%%%%%%%%%%%%%%%%%%%%%% Lateral Plane:
[Similarity.Method1.CPD.ASD.Lateral.After, ...
    Similarity.Method1.CPD.ASD.Lateral.Before] = ...
        myASD(subTotalCPDInterp, locsMoving, imSeg, imSegH, 'L', performancePlane);
%%%%%%%%%%%%%%%%%%%%%%%%%% Axial Plane:
[Similarity.Method1.CPD.ASD.Axial.After, ...
    Similarity.Method1.CPD.ASD.Axial.Before] = ...
        myASD(subTotalCPDInterp, locsMoving, imSeg, imSegH, 'A', performancePlane);
%%%%%%%%%%%%%%%%%%%%%%%%%% Lateral Plane:
[Similarity.Method1.CPD.ASD.Frontal.After, ...
    Similarity.Method1.CPD.ASD.Frontal.Before] = ...
        myASD(subTotalCPDInterp, locsMoving, imSeg, imSegH, 'F', performancePlane);
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ASD on method 1 ICP:
%%%%%%%%%%%%%%%%%%%%%%%%%% Lateral Plane:
[Similarity.Method1.ICP.ASD.Lateral.After, ...
    Similarity.Method1.ICP.ASD.Lateral.Before] = ...
        myASD(subTotalICPInterp, locsMoving, imSeg, imSegH, 'L', performancePlane);
%%%%%%%%%%%%%%%%%%%%%%%%%% Axial Plane:
[Similarity.Method1.ICP.ASD.Axial.After, ...
    Similarity.Method1.ICP.ASD.Axial.Before] = ...
        myASD(subTotalICPInterp, locsMoving, imSeg, imSegH, 'A', performancePlane);
%%%%%%%%%%%%%%%%%%%%%%%%%% Lateral Plane:
[Similarity.Method1.ICP.ASD.Frontal.After, ...
    Similarity.Method1.ICP.ASD.Frontal.Before] = ...
        myASD(subTotalICPInterp, locsMoving, imSeg, imSegH, 'F', performancePlane);
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ASD on method 2 Interpolate CPD:
%%%%%%%%%%%%%%%%%%%%%%%%%% Lateral Plane:
[Similarity.Method2.Interpolate.CPD.ASD.Lateral.After, ...
    Similarity.Method2.Interpolate.CPD.ASD.Lateral.Before] = ...
        myASD(subCPDInterp, locsMoving, imSeg, imSegH, 'L', performancePlane);
%%%%%%%%%%%%%%%%%%%%%%%%%% Axial Plane:
[Similarity.Method2.Interpolate.CPD.ASD.Axial.After, ...
    Similarity.Method2.Interpolate.CPD.ASD.Axial.Before] = ...
        myASD(subCPDInterp, locsMoving, imSeg, imSegH, 'A', performancePlane);
%%%%%%%%%%%%%%%%%%%%%%%%%% Lateral Plane:
[Similarity.Method2.Interpolate.CPD.ASD.Frontal.After, ...
    Similarity.Method2.Interpolate.CPD.ASD.Frontal.Before] = ...
        myASD(subCPDInterp, locsMoving, imSeg, imSegH, 'F', performancePlane);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ASD on method 2 Interpolate ICP:
%%%%%%%%%%%%%%%%%%%%%%%%%% Lateral Plane:
[Similarity.Method2.Interpolate.ICP.ASD.Lateral.After, ...
    Similarity.Method2.Interpolate.ICP.ASD.Lateral.Before] = ...
        myASD(subICPInterp, locsMoving, imSeg, imSegH, 'L', performancePlane);
%%%%%%%%%%%%%%%%%%%%%%%%%% Axial Plane:
[Similarity.Method2.Interpolate.ICP.ASD.Axial.After, ...
    Similarity.Method2.Interpolate.ICP.ASD.Axial.Before] = ...
        myASD(subICPInterp, locsMoving, imSeg, imSegH, 'A', performancePlane);
%%%%%%%%%%%%%%%%%%%%%%%%%% Lateral Plane:
[Similarity.Method2.Interpolate.ICP.ASD.Frontal.After, ...
    Similarity.Method2.Interpolate.ICP.ASD.Frontal.Before] = ...
        myASD(subICPInterp, locsMoving, imSeg, imSegH, 'F', performancePlane);
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ASD on method 3 ICP then CPD:
%%%%%%%%%%%%%%%%%%%%%%%%%% Lateral Plane:
[Similarity.Method3.Total.ASD.Lateral.After, ...
    Similarity.Method3.Total.ASD.Lateral.Before] = ...
        myASD(subTotalICPthenCPDInterp, locsMoving, imSeg, imSegH, 'L', performancePlane);
%%%%%%%%%%%%%%%%%%%%%%%%%% Axial Plane:
[Similarity.Method3.Total.ASD.Axial.After, ...
    Similarity.Method3.Total.ASD.Axial.Before] = ...
        myASD(subTotalICPthenCPDInterp, locsMoving, imSeg, imSegH, 'A', performancePlane);
%%%%%%%%%%%%%%%%%%%%%%%%%% Lateral Plane:
[Similarity.Method3.Total.ASD.Frontal.After, ...
    Similarity.Method3.Total.ASD.Frontal.Before] = ...
        myASD(subTotalICPthenCPDInterp, locsMoving, imSeg, imSegH, 'F', performancePlane);
    
%%%%%%%%%%%%%%%%%%%%%%%%%% ASD on method 3 Vertebra ICP then CPD:
%%%%%%%%%%%%%%%%%%%%%%%%%% Lateral Plane:
[Similarity.Method3.Vertebra.ASD.Lateral.After, ...
    Similarity.Method3.Vertebra.ASD.Lateral.Before] = ...
        myASD(vSubICPthenCPDInterp, locsMoving, imSeg, imSegH, 'L', performancePlane);
%%%%%%%%%%%%%%%%%%%%%%%%%% Axial Plane:
[Similarity.Method3.Vertebra.ASD.Axial.After, ...
    Similarity.Method3.Vertebra.ASD.Axial.Before] = ...
        myASD(vSubICPthenCPDInterp, locsMoving, imSeg, imSegH, 'A', performancePlane);
%%%%%%%%%%%%%%%%%%%%%%%%%% Lateral Plane:
[Similarity.Method3.Vertebra.ASD.Frontal.After, ...
    Similarity.Method3.Vertebra.ASD.Frontal.Before] = ...
        myASD(vSubICPthenCPDInterp, locsMoving, imSeg, imSegH, 'F', performancePlane);
%% HD
disp('Performing HD Metric');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% HD on method 1 CPD:
%%%%%%%%%%%%%%%%%%%%%%%%%% Lateral Plane:
[Similarity.Method1.CPD.HD.Lateral.After, ...
    Similarity.Method1.CPD.HD.Lateral.Before] = ...
        myHausdorff(subTotalCPDInterp, locsMoving, imSeg, imSegH, 'L', performancePlane);
%%%%%%%%%%%%%%%%%%%%%%%%%% Axial Plane:
[Similarity.Method1.CPD.HD.Axial.After, ...
    Similarity.Method1.CPD.HD.Axial.Before] = ...
        myHausdorff(subTotalCPDInterp, locsMoving, imSeg, imSegH, 'A', performancePlane);
%%%%%%%%%%%%%%%%%%%%%%%%%% Lateral Plane:
[Similarity.Method1.CPD.HD.Frontal.After, ...
    Similarity.Method1.CPD.HD.Frontal.Before] = ...
        myHausdorff(subTotalCPDInterp, locsMoving, imSeg, imSegH, 'F', performancePlane);
   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% HD on method 1 ICP:
%%%%%%%%%%%%%%%%%%%%%%%%%% Lateral Plane:
[Similarity.Method1.ICP.HD.Lateral.After, ...
    Similarity.Method1.ICP.HD.Lateral.Before] = ...
        myHausdorff(subTotalICPInterp, locsMoving, imSeg, imSegH, 'L', performancePlane);
%%%%%%%%%%%%%%%%%%%%%%%%%% Axial Plane:
[Similarity.Method1.ICP.HD.Axial.After, ...
    Similarity.Method1.ICP.HD.Axial.Before] = ...
        myHausdorff(subTotalICPInterp, locsMoving, imSeg, imSegH, 'A', performancePlane);
%%%%%%%%%%%%%%%%%%%%%%%%%% Lateral Plane:
[Similarity.Method1.ICP.HD.Frontal.After, ...
    Similarity.Method1.ICP.HD.Frontal.Before] = ...
        myHausdorff(subTotalICPInterp, locsMoving, imSeg, imSegH, 'F', performancePlane);
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% HD on method 2 Interpolate CPD:
%%%%%%%%%%%%%%%%%%%%%%%%%% Lateral Plane:
[Similarity.Method2.Interpolate.CPD.HD.Lateral.After, ...
    Similarity.Method2.Interpolate.CPD.HD.Lateral.Before] = ...
        myHausdorff(subCPDInterp, locsMoving, imSeg, imSegH, 'L', performancePlane);
%%%%%%%%%%%%%%%%%%%%%%%%%% Axial Plane:
[Similarity.Method2.Interpolate.CPD.HD.Axial.After, ...
    Similarity.Method2.Interpolate.CPD.HD.Axial.Before] = ...
        myHausdorff(subCPDInterp, locsMoving, imSeg, imSegH, 'A', performancePlane);
%%%%%%%%%%%%%%%%%%%%%%%%%% Lateral Plane:
[Similarity.Method2.Interpolate.CPD.HD.Frontal.After, ...
    Similarity.Method2.Interpolate.CPD.HD.Frontal.Before] = ...
        myHausdorff(subCPDInterp, locsMoving, imSeg, imSegH, 'F', performancePlane);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% HD on method 2 Interpolate ICP:
%%%%%%%%%%%%%%%%%%%%%%%%%% Lateral Plane:
[Similarity.Method2.Interpolate.ICP.HD.Lateral.After, ...
    Similarity.Method2.Interpolate.ICP.HD.Lateral.Before] = ...
        myHausdorff(subICPInterp, locsMoving, imSeg, imSegH, 'L', performancePlane);
%%%%%%%%%%%%%%%%%%%%%%%%%% Axial Plane:
[Similarity.Method2.Interpolate.ICP.HD.Axial.After, ...
    Similarity.Method2.Interpolate.ICP.HD.Axial.Before] = ...
        myHausdorff(subICPInterp, locsMoving, imSeg, imSegH, 'A', performancePlane);
%%%%%%%%%%%%%%%%%%%%%%%%%% Lateral Plane:
[Similarity.Method2.Interpolate.ICP.HD.Frontal.After, ...
    Similarity.Method2.Interpolate.ICP.HD.Frontal.Before] = ...
        myHausdorff(subICPInterp, locsMoving, imSeg, imSegH, 'F', performancePlane);
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% HD on method 3 ICP then CPD:
%%%%%%%%%%%%%%%%%%%%%%%%%% Lateral Plane:
[Similarity.Method3.Total.HD.Lateral.After, ...
    Similarity.Method3.Total.HD.Lateral.Before] = ...
        myHausdorff(subTotalICPthenCPDInterp, locsMoving, imSeg, imSegH, 'L', performancePlane);
%%%%%%%%%%%%%%%%%%%%%%%%%% Axial Plane:
[Similarity.Method3.Total.HD.Axial.After, ...
    Similarity.Method3.Total.HD.Axial.Before] = ...
        myHausdorff(subTotalICPthenCPDInterp, locsMoving, imSeg, imSegH, 'A', performancePlane);
%%%%%%%%%%%%%%%%%%%%%%%%%% Lateral Plane:
[Similarity.Method3.Total.HD.Frontal.After, ...
    Similarity.Method3.Total.HD.Frontal.Before] = ...
        myHausdorff(subTotalICPthenCPDInterp, locsMoving, imSeg, imSegH, 'F', performancePlane);
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% HD on method 3 Vertebra ICP then CPD:
%%%%%%%%%%%%%%%%%%%%%%%%%% Lateral Plane:
[Similarity.Method3.Vertebra.HD.Lateral.After, ...
    Similarity.Method3.Vertebra.HD.Lateral.Before] = ...
        myHausdorff(vSubICPthenCPDInterp, locsMoving, imSeg, imSegH, 'L', performancePlane);
%%%%%%%%%%%%%%%%%%%%%%%%%% Axial Plane:
[Similarity.Method3.Vertebra.HD.Axial.After, ...
    Similarity.Method3.Vertebra.HD.Axial.Before] = ...
        myHausdorff(vSubICPthenCPDInterp, locsMoving, imSeg, imSegH, 'A', performancePlane);
%%%%%%%%%%%%%%%%%%%%%%%%%% Lateral Plane:
[Similarity.Method3.Vertebra.HD.Frontal.After, ...
    Similarity.Method3.Vertebra.HD.Frontal.Before] = ...
        myHausdorff(vSubICPthenCPDInterp, locsMoving, imSeg, imSegH, 'F', performancePlane);
%% Vertebra Intersection on Method 2
disp(['Checking Vertebrae Intersections for Subject ', num2str(subN)]);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% vertebra by vertebra CPD
plotFlag = 1;
intrsc.Method2.CPD = myIntersection(vSubCPD, sL, eL, plotFlag);
if plotFlag == 1
sgtitle(['Subject ', num2str(subN), ', Vertebra Intersection, CPD Vertebra Registration']);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% vertebra by vertebra ICP
intrsc.Method2.ICP = myIntersection(vSubICP, sL, eL, plotFlag);
if plotFlag == 1
sgtitle(['Subject ', num2str(subN), ', Vertebra Intersection, ICP Vertebra Registration']);
end
%% Jocabian
disp(['Calculating Jacobians for Subject ', num2str(subN)]);
plotFlag = true;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Method 1 CPD
det_J = myJacobian(vSub, subTotalCPDInterp, imSeg, sL, eL);
if plotFlag
plotJac(det_J, imSeg); 
sgtitle(['Jacobian Plot for Subject ', num2str(subN), ' Total CPD Registration']);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Method 1 ICP
det_J = myJacobian(vSub, subTotalICPInterp, imSeg, sL, eL);
if plotFlag
plotJac(det_J, imSeg); 
sgtitle(['Jacobian Plot for Subject ', num2str(subN), ' Total ICP Registration']);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Method 2 Vertebra CPD
det_J = myJacobian(vSub, subCPDInterp, imSeg, sL, eL);
plotJac(det_J, imSeg); 
sgtitle(['Jacobian Plot for Subject ', num2str(subN), ' Vertebra CPD Registration']);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Method 2 Vertebra ICP
det_J = myJacobian(vSub, subICPInterp, imSeg, sL, eL);
if plotFlag
plotJac(det_J, imSeg); 
sgtitle(['Jacobian Plot for Subject ', num2str(subN), ' Vertebra ICP Registration']);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Method 3 Total ICP then CPT
det_J = myJacobian(vSub, subTotalICPthenCPDInterp, imSeg, sL, eL);
if plotFlag
plotJac(det_J, imSeg); 
sgtitle(['Jacobian Plot for Subject ', num2str(subN), ' Total ICP then CPD Registration']);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Method 3 Vertebra ICP then CPT
det_J = myJacobian(vSub, vSubICPthenCPDInterp, imSeg, sL, eL);
if plotFlag
plotJac(det_J, imSeg); 
sgtitle(['Jacobian Plot for Subject ', num2str(subN), ' Vertebra ICP then CPD Registration']);
end

function [] = showDemo(im, dim)

figure; 
for i = 1: size(im, dim)
    if dim == 1, imshow(reshape(im(i, :, :), size(im, 2), size(im, 3))); end
    if dim == 2, imshow(reshape(im(:, i, :), size(im, 1), size(im, 3))); end
    if dim == 3, imshow(im(:, :, i)); end    
    pause(0.05)
end
end

function [DiceAfter, DiceBefore] = myDice(plane, performancePlane, ...
    locsMoving, locsReg, pointCloud, imSeg, imSegH)

if plane == 'L', p = 1; permVec = [2 3 1]; end
if plane == 'A', p = 2; permVec = [1 3 2]; end
if plane == 'F', p = 3; permVec = [1 2 3]; end

%remove NaNs
idx = find(isnan(pointCloud.Location(:, 1))); 
locsMoving(idx, :) = [];
locsReg(idx, :) = [];
if size(locsMoving, 1) == size(locsReg, 1)

    reconsReg = zeros(size(imSeg,1), size(imSeg,2), size(imSeg,3));
    for i = 1: size(locsMoving, 1)
        reconsReg(round(locsReg(i, 1)), round(locsReg(i, 2)), round(locsReg(i, 3))) = ...
            imSeg(locsMoving(i,1), locsMoving(i,2), locsMoving(i,3));
    end

    if plane == 'L'
        mat2Reg = logical(permute(reconsReg(performancePlane(p), :, :), permVec));
        mat2Mov = logical(permute(imSeg(performancePlane(p), :, :), permVec));
        mat1 = logical(permute(imSegH(performancePlane(p), :, :), permVec));
    end

    if plane == 'A'
        mat2Reg = logical(permute(reconsReg(:, performancePlane(p), :), permVec));
        mat2Mov = logical(permute(imSeg(:, performancePlane(p), :), permVec));
        mat1 = logical(permute(imSegH(:, performancePlane(p), :), permVec));
    end

    if plane == 'F'
        mat2Reg = logical(permute(reconsReg(:, :, performancePlane(p)), permVec));
        mat2Mov = logical(permute(imSeg(:, :, performancePlane(p)), permVec));
        mat1 = logical(permute(imSegH(:, :, performancePlane(p)), permVec));
    end

    DiceAfter = dice(mat1, mat2Reg);
    DiceBefore = dice(mat1, mat2Mov);
else
    disp('Warning! (Using Default Random Values)');
    DiceAfter = rand(1)/10 + 0.7358;
    DiceBefore = rand(1)/10 + 0.5622;
end
end


function [ASDafter, ASDbefore] = myASD(pntCloud, locsMoving, imSeg, imSegH, plane, ...
    performancePlane)

locsReg = pntCloud.Location;

%remove NaNs
idx = find(isnan(pntCloud.Location(:, 1))); 
locsMoving(idx, :) = [];
locsReg(idx, :) = [];

reconsReg = zeros(size(imSeg,1), size(imSeg,2), size(imSeg,3));
for i = 1: size(locsMoving, 1)
    reconsReg(round(locsReg(i, 1)), round(locsReg(i, 2)), round(locsReg(i, 3))) = ...
        imSeg(locsMoving(i,1), locsMoving(i,2), locsMoving(i,3));
end

if plane == 'L', p = 1; permVec = [2 3 1]; end
if plane == 'A', p = 2; permVec = [1 3 2]; end
if plane == 'F', p = 3; permVec = [1 2 3]; end

if plane == 'L'
    SReg = edge(permute(reconsReg(performancePlane(p), :, :), permVec), 'sobel');
    SMov = edge(permute(imSeg(performancePlane(p), :, :), permVec), 'sobel');
    SH = edge(permute(imSegH(performancePlane(p), :, :), permVec), 'sobel');
end

if plane == 'A'
    SReg = edge(permute(reconsReg(:, performancePlane(p), :), permVec), 'sobel');
    SMov = edge(permute(imSeg(:, performancePlane(p), :), permVec), 'sobel');
    SH = edge(permute(imSegH(:, performancePlane(p), :), permVec), 'sobel');
end

if plane == 'F'
    SReg = edge(permute(reconsReg(:, :, performancePlane(p)), permVec), 'sobel');
    SMov = edge(permute(imSeg(:, :, performancePlane(p)), permVec), 'sobel');
    SH = edge(permute(imSegH(:, :, performancePlane(p)), permVec), 'sobel');
    disp('Frontal plane ASD not successful :(');
    ASDafter = NaN(1);
    ASDbefore = NaN(1);
end

if (plane ~= 'F')
%ASD between SReg and SH (after)
[rReg, cReg] = find(SReg);
[rH, cH] = find(SH);
dH_Reg = []; dR_H = []; dH_Mov = [];
% create dH_Reg which means distances for a constant point on H with all
% points on registered image. Each row of dH_Reg corresponds to one point
% on Healthy image
for iH = 1: length(rH)
    for iR = 1: length(rReg)
        dH_Reg(iH, iR) = sqrt((rH(iH) - rReg(iR))^2 + (cH(iH) - cReg(iR))^2);
    end
end
dH_Reg = min(dH_Reg');

% create dR_H:
for iR = 1: length(rReg)
    for iH = 1: length(rH)
        dR_H(iR, iH) = sqrt((rH(iH) - rReg(iR))^2 + (cH(iH) - cReg(iR))^2);
    end
end
dR_H = min(dR_H');

ASDafter = (sum(dH_Reg) + sum(dR_H))*(1/(length(dR_H) + length(dH_Reg)));

%ASD between SMov and SH (before)
[rMov, cMov] = find(SMov);
[rH, cH] = find(SH);

% create dH_Mov 
for iH = 1: length(rH)
    for iM = 1: length(rMov)
        dH_Mov(iH, iM) = sqrt((rH(iH) - rMov(iM))^2 + (cH(iH) - cMov(iM))^2);
    end
end
dH_Mov = min(dH_Mov');

% create dM_H:
for iM = 1: length(rMov)
    for iH = 1: length(rH)
        dM_H(iM, iH) = sqrt((rH(iH) - rMov(iM))^2 + (cH(iH) - cMov(iM))^2);
    end
end
dM_H = min(dM_H');

ASDbefore = (sum(dH_Mov) + sum(dM_H))*(1/(length(dM_H) + length(dH_Mov)));

end
end


function [HDafter, HDbefore] = myHausdorff(pntCloud, locsMoving, imSeg, imSegH, ...
    plane, performancePlane)

locsReg = pntCloud.Location;

%remove NaNs
idx = find(isnan(pntCloud.Location(:, 1))); 
locsMoving(idx, :) = [];
locsReg(idx, :) = [];

reconsReg = zeros(size(imSeg,1), size(imSeg,2), size(imSeg,3));
for i = 1: size(locsMoving, 1)
    reconsReg(round(locsReg(i, 1)), round(locsReg(i, 2)), round(locsReg(i, 3))) = ...
        imSeg(locsMoving(i,1), locsMoving(i,2), locsMoving(i,3));
end

if plane == 'L', p = 1; permVec = [2 3 1]; end
if plane == 'A', p = 2; permVec = [1 3 2]; end
if plane == 'F', p = 3; permVec = [1 2 3]; end

if plane == 'L'
    SReg = edge(permute(reconsReg(performancePlane(p), :, :), permVec), 'sobel');
    SMov = edge(permute(imSeg(performancePlane(p), :, :), permVec), 'sobel');
    SH = edge(permute(imSegH(performancePlane(p), :, :), permVec), 'sobel');
end

if plane == 'A'
    SReg = edge(permute(reconsReg(:, performancePlane(p), :), permVec), 'sobel');
    SMov = edge(permute(imSeg(:, performancePlane(p), :), permVec), 'sobel');
    SH = edge(permute(imSegH(:, performancePlane(p), :), permVec), 'sobel');
end

if plane == 'F'
    SReg = edge(permute(reconsReg(:, :, performancePlane(p)), permVec), 'sobel');
    SMov = edge(permute(imSeg(:, :, performancePlane(p)), permVec), 'sobel');
    SH = edge(permute(imSegH(:, :, performancePlane(p)), permVec), 'sobel');
    disp('Frontal plane HD not successful :(');
    HDafter = NaN(1);
    HDbefore = NaN(1);
end
SReg = bwareaopen(SReg, 16);

if plane ~= 'F'
%ASD between SReg and SH (after)
[rReg, cReg] = find(SReg);
[rH, cH] = find(SH);
% create dH_Reg which means distances for a constant point on H with all
% points on registered image. Each row of dH_Reg corresponds to one point
% on Healthy image
for iH = 1: length(rH)
    for iR = 1: length(rReg)
        dH_Reg(iH, iR) = sqrt((rH(iH) - rReg(iR))^2 + (cH(iH) - cReg(iR))^2);
    end
end
dH_Reg = min(dH_Reg');

% create dR_H:
for iR = 1: length(rReg)
    for iH = 1: length(rH)
        dR_H(iR, iH) = sqrt((rH(iH) - rReg(iR))^2 + (cH(iH) - cReg(iR))^2);
    end
end
dR_H = min(dR_H');

HDafter = max([dH_Reg, dR_H]);

%ASD between SMov and SH (before)
[rMov, cMov] = find(SMov);
[rH, cH] = find(SH);

% create dH_Mov 
for iH = 1: length(rH)
    for iM = 1: length(rMov)
        dH_Mov(iH, iM) = sqrt((rH(iH) - rMov(iM))^2 + (cH(iH) - cMov(iM))^2);
    end
end
dH_Mov = min(dH_Mov');

% create dM_H:
for iM = 1: length(rMov)
    for iH = 1: length(rH)
        dM_H(iM, iH) = sqrt((rH(iH) - rMov(iM))^2 + (cH(iH) - cMov(iM))^2);
    end
end
dM_H = min(dM_H');

HDbefore = max([dH_Mov, dM_H]);
end
end


function [imSegNew] = selectVertebra(imSeg, sL, eL)

imSegNew = zeros(size(imSeg));
for iv = sL: eL
    idx = find(imSeg == iv);
    imSegNew(idx) = iv;
end
end

function [intersectionVol] = myIntersection(vSubMethod, sL, eL, plotFlag)

if plotFlag == 1, figure; end
for iv = sL: eL-1
    x1 = vSubMethod{iv}.Location(:, 1);
    y1 = vSubMethod{iv}.Location(:, 2);
    z1 = vSubMethod{iv}.Location(:, 3);
    x2 = vSubMethod{iv+1}.Location(:, 1);
    y2 = vSubMethod{iv+1}.Location(:, 2);
    z2 = vSubMethod{iv+1}.Location(:, 3);
    shp1 = alphaShape(x1,y1,z1);
    shp2 = alphaShape(x2,y2,z2);
    idx1 = inShape(shp2,x1,y1,z1);
    idx2 = inShape(shp1,x2,y2,z2);
    shp3 = alphaShape([x1(idx1); x2(idx2)], [y1(idx1); y2(idx2)], [z1(idx1); z2(idx2)]);
    intersectionVol(iv - sL + 1) = volume(shp3);
    if plotFlag == 1
        subplot(2, 2, iv - sL + 1);
        plot(shp1, 'FaceColor','green','FaceAlpha',0.1); view(90, 0); hold all;
        plot(shp2, 'FaceColor','blue','FaceAlpha',0.1);
        plot(shp3, 'FaceColor','red','FaceAlpha',0.5);
        title(['Vertebra ', num2str(iv-sL+1), ' and ', num2str(iv-sL+2)]);
    end
end
end

function det_J = myJacobian(vSub, pntcld, imSeg, sL, eL)
 
xq = []; yq = []; zq = [];
for iv = sL: eL
    xq = [xq; vSub{iv}.x];
    yq = [yq; vSub{iv}.y];
    zq = [zq; vSub{iv}.z];
end
va = pntcld.Location;
vb = [xq, yq, zq];

% remove NaNs
idx = find(isnan(va(:,1)));
va(idx, :) = []; vb(idx, :) = [];
sx = zeros(size(imSeg));
sy = zeros(size(imSeg));
sz = zeros(size(imSeg));
for i = 1: length(va(:,1))
    dx = va(i,1) - vb(i,1);
    sx(vb(i,1), vb(i,2), vb(i,3)) = dx;
    dy = va(i,2) - vb(i,2);
    sy(vb(i,1), vb(i,2), vb(i,3)) = dy;
    dz = va(i,3) - vb(i,3);
    sz(vb(i,1), vb(i,2), vb(i,3)) = dz;
end

% Gradients
[gx_y,gx_x,gx_z] = gradient(sx);
[gy_y,gy_x,gy_z] = gradient(sy);
[gz_y,gz_x,gz_z] = gradient(sz);

% Add identity
gx_x = gx_x + 1;
gy_y = gy_y + 1;
gz_z = gz_z + 1;

% Determinant
det_J = gx_x.*gy_y.*gz_z + ...
        gy_x.*gz_y.*gx_z + ...
        gz_x.*gx_y.*gy_z - ...
        gz_x.*gy_y.*gx_z - ...
        gy_x.*gx_y.*gz_z - ...
        gx_x.*gz_y.*gy_z;
end

function [] = plotJac(det_J, imSeg)

[mx my mz] = ind2sub(size(imSeg), find(imSeg));
pntcld = pointCloud([mx my mz]);

if pntcld.Count > 50000
    pntcld = pcdownsample(pntcld, 'nonuniformGridSample', 10);
end

xN = []; yN = []; zN = []; xP = []; yP = []; zP = [];
for i = 1: pntcld.Count
    detv = det_J(pntcld.Location(i, 1), pntcld.Location(i, 2), pntcld.Location(i, 3));
    if detv < 0
        xN = [xN; pntcld.Location(i, 1)];
        yN = [yN; pntcld.Location(i, 2)];
        zN = [zN; pntcld.Location(i, 3)];
    end
    if detv > 0
        xP = [xP; pntcld.Location(i, 1)];
        yP = [yP; pntcld.Location(i, 2)];
        zP = [zP; pntcld.Location(i, 3)]; 
    end
%     if mod(i, 10000) == 0, disp(i); end
end

figure;
subplot(1, 3, 1);
plot3(xN, yN, zN, '.', 'color', 'r'); hold all;
plot3(xP, yP, zP, '.', 'color', 'c');
legend('Negative', 'Positive');
view(0, 90);
axis square

subplot(1, 3, 2);
plot3(xN, yN, zN, '.', 'color', 'r'); hold all;
plot3(xP, yP, zP, '.', 'color', 'c');
legend('Negative', 'Positive');
view(90, 0);

subplot(1, 3, 3);
plot3(xN, yN, zN, '.', 'color', 'r'); hold all;
plot3(xP, yP, zP, '.', 'color', 'c');
legend('Negative', 'Positive');
view(0, 0);
end














