# Medical Image Registration

- Registering patients' CT images of spinal cord with a healthy subject image (atlas) using Coherent Point Drift (CPD) and Iterative Closest Point (ICP) registration algorithms.

- Extended the standard ICP and CPD methods to achieve an algorithm for spinal cord images' registration in particular.

- Tested registration performance by Dice Score, Average Surface Distance, Hausdorff Distance, Vertebra Intersection amount, and Jacobian determinant.
