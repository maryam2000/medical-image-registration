%% introduction
this code is provided for MIAP Spring 2021 Term Project Dr. Fatemizadeh "Image Registration"
Student Name: Maryam Maghsoudi Shaghaghi
Student Number: 97102503
---------------------------------------------------------------------------------------------------------------------------
%%
The code contains three main parts which are seperated by %% sign with all the letters written Capital:
1) PREPARATION:
2) REGISTRATION METHODS:
3) PERFORMANCE METRICES:
---------------------------------------------------------------------------------------------------------------------------
%% PREPARATION:
PREPARATION part includes:
1.1) Load the data
1.2) Select L1-L5 vertebrae data points
1.3) Seperate data of each vertebrae into a cell array
1.4) Extract boundry points
1.5) Derive PointClouds
This part does not take a long running time.
---------------------------------------------------------------------------------------------------------------------------
%% REGISTRATION METHODS:
Three type of registration methods are implemented in this code.
Each section (seperated by %% sign) in the REGISTRATION METHODS part start with "Method 1:", "Method 2:", or "Method 3:".
2.1) METHOD 1:
2.1.01) %% CPD Total Registration
	Implements CPD on the whole subject image. 
	ATTENTION: This part takes a very long time to run but is required for the next sections.
	I suggest you to save the "subCPD.mat" matrix at the end of running and load it for the next sections.
2.1.02) %% Interpolate Total CPD
	Interpolates the inside points of subject image according to the transform derived from the previous section.
	ATTENTION: This part takes a long run time.
	I suggest you to save the "subTotalCPDInterp.mat" matrix at the end of running and load it for the next sections.
2.1.03) %% plot Total CPD results
	Simply run this part to see the registration output of the previous 2 sections (2 Figures).
2.1.04) %% CPD Total Registration
	Similar to 2.1.1 but for ICP - "subICP.mat"
2.1.05) %% Interpolate Total ICP
	Similar to 2.1.2 but for ICP - "subTotalICPInterp.mat"
2.1.06) plot Total ICP results
	Similar to 2.1.3 but for ICP
---------------------------------------------------------------------------------------------------------------------------
2.2) METHOD 2:
2.2.01) %% CPD vertebra registration
	Extracts each vertebrae and applies CPD on each. - "vSubCPD.mat"
2.2.02) %% plot CPD results per vertebra
	Simply run this part. (I recommend you to comment this section since it does not provide any specific demonstration)
2.2.03) %% Reconstruct the spine from CPD registered vertebra - "spineCPDRegPC.mat"
	Puts the registered vertebras onto each other.
2.2.04) %% plot CPD vertebra registration reconstruction results
	plots the previous section result. Simply run this part.
2.2.05) %% vertebra CPD interpolation
	Interpolates the inside points of subject image according to the transform derived from the previous section.
	ATTENTION: This part takes a long run time.
	I suggest you to save the "SubCPDInterp.mat" matrix at the end of running and load it for the next sections.
2.2.06) %% ICP vertebra registration
	Similar to 2.2.1 but for ICP - "vSubICP.mat"
2.2.07) %% plot ICP results per vertebra
	Similar to 2.2.2 but for ICP. (I recommend you to comment this section since it does not provide any specific demonstration)
2.2.08) %% Reconstruct the spine from ICP registered vertebra - "spineICPRegPC.mat"
	Similar to 2.2.3 but for ICP
2.2.09) %% plot CPD vertebra registration reconstruction results
	Similar to 2.2.4 but for ICP
2.2.10) %% vertebra ICP interpolation
	ATTENTION: This part takes a long run time.
	I suggest you to save the "SubICPInterp.mat" matrix at the end of running and load it for the next sections.
---------------------------------------------------------------------------------------------------------------------------	
2.3) METHOD 3:
This method is based on the idea of implementing ICP first, which is an affine transform, and applying CPD next.
This method contains two parts. First, ICPthenCPD is impelemented on overall image (like method 1). Second, ICPthenCPD
is implemented on each vertebrae seperately and then the total image is interpolated.
2.3.01) %% Total ICP then CPD
	Applies ICP on overall subject image, and then applies CPD on that - "subTotalICPthenCPD.mat"
2.3.02) %% Interpolate Total ICP then CPD
	Interpolates the previous sections for all the image points. - "subTotalICPthenCPDInterp.mat"
	ATTENTION: This section takes a long run time.
2.3.03) %% Vertebra ICP then CPD registration
	Impelements ICP then CPD on each vertebrae seperately. - "vSubICPthenCPD.mat"
2.3.04) %% Interpolate vertebra ICP then CPD
	Interpolates the previous sections for all the image points. - "vSubICPthenCPDInterp.mat"
	ATTENTION: This section takes a long run time.
2.3.05) %% plot vertebra ICP then CPD registration interpolation
	Simply run this part.
* The idea of method 3 has developed with cooperation of Ali Ghavam
---------------------------------------------------------------------------------------------------------------------------
%% PERFORMANCE METRICES:
3.01) %% Extract Performance Planes
	Extracts three lateral, axial, frontal planes from Healthy Image to apply performance metrices on them
3.02) %% DS
	Dice Score - Uses "myDice.m" function.
3.03) %% ASD
	Average Surface Distance - Uses "myASD.m" function.
3.04) %% HD
	Hausdorff Distance - Uses "myHausdorff.m" function.
ATTENTION: The results of sections 3.02, 3.03, and 3.04 are stored in a "Similarity" struct for each method.
3.05) %% Vertebra Intersection on Method 2 
	Calculates the value of vertebra intersection - Uses "myIntersection.m" function.
	Set plotFlag = 1, to observe the results.
	ATTENTION: The results of this section are stored in "intrsc" struct.
3.06) %% Jocabian
	Calculates Jacobian matrix for each method - Uses "myJacobian.m" function.
	Set plotFlag = true to observe the results.
---------------------------------------------------------------------------------------------------------------------------